(function(BEM) {

  BEM.DOM.decl('search-form', {
    onSetMod: {
      'js': {
        'inited': function () {
          this.findBlockInside('search')
            .on('search-happened', this._onSeachHappened, this);
        }
      }
    },
    
    _onSeachHappened: function(e, text) {
      this.elem('results').append(
        $('<div/>').text(text)
      );
    }
  });

})(BEM);