(function(BEM) {

  BEM.DOM.decl('search', {
    onSetMod: {
      'js': {
        'inited': function () {
          this.bindTo('button', 'click', this._onSearchClicked);
        }
      },
      
      'disabled': {
        'true': function() {
          this.setMod(this.elem('input'), 'disabled');
        },
        
        '': function() {
          this.delMod(this.elem('input'), 'disabled');
        }
      }
    },
    
    _onSearchClicked: function(e) {
      this.emit('search-happened', this.elem('input').val());
    }
  });

})(BEM);